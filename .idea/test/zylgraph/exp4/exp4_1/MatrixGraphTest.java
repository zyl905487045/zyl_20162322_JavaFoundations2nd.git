package zylgraph.exp4.exp4_1;

import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/11/20.
 */

public class MatrixGraphTest extends TestCase{
    MatrixGraph matrixGraph = new MatrixGraph(10);
    public void test1(){
        /**
         * 测试向两个相同的顶点添加边
         */
        assertEquals(false, matrixGraph.addEdge(1, 1));
        assertEquals(false, matrixGraph.isEdgeExists(1, 1));
    }

    public void test2(){
        /**
         * 测试向不存在的点添加边
         */
        assertEquals(false, matrixGraph.addEdge(-1, 1));
        assertEquals(false, matrixGraph.isEdgeExists(-1, 1));
        assertEquals(false, matrixGraph.addEdge(1, 15));
        assertEquals(false, matrixGraph.isEdgeExists(1, 15));
        assertEquals(false, matrixGraph.addEdge(-1, 15));
        assertEquals(false, matrixGraph.isEdgeExists(-1, 15));
    }

    public void test3(){
        /**
         * 测试向两个合理的点添加边
         */
        assertEquals(true, matrixGraph.addEdge(1,6));
        assertEquals(true, matrixGraph.isEdgeExists(1, 6));
    }

    public void test4(){
        /**
         * 测试向边缘的点添加边
         *
         */
        assertEquals(true, matrixGraph.addEdge(0, 9));
        assertEquals(true, matrixGraph.isEdgeExists(0, 9));
    }

    public void test5(){
        /**
         * 测试向边缘的点添加边
         *
         */
        assertEquals(true, matrixGraph.addEdge(0, 9));
        assertEquals(true, matrixGraph.isEdgeExists(0, 9));
    }

    public void test6(){
        /**
         * 测试无向图由位置大的点向位置小的点添加
         */
        assertEquals(true, matrixGraph.addEdge(8, 4));
        assertEquals(true, matrixGraph.isEdgeExists(8, 4));
    }
    public void testbsf() {
        MatrixGraph matrixGraph = new MatrixGraph(6);
        matrixGraph.addEdge(0, 1);
        matrixGraph.addEdge(0, 5);
        matrixGraph.addEdge(0, 2);
        matrixGraph.addEdge(1, 2);
        matrixGraph.addEdge(2, 3);
        matrixGraph.addEdge(1, 4);
        matrixGraph.addEdge(2, 4);
        matrixGraph.bsf();
    }

    public void testdfs() throws Exception {
        MatrixGraph matrixGraph = new MatrixGraph(6);
        matrixGraph.addEdge(0, 1);
        matrixGraph.addEdge(0, 5);
        matrixGraph.addEdge(0, 2);
        matrixGraph.addEdge(1, 2);
        matrixGraph.addEdge(2, 3);
        matrixGraph.addEdge(1, 4);
        matrixGraph.addEdge(2, 4);
        matrixGraph.dfs();
    }

}
