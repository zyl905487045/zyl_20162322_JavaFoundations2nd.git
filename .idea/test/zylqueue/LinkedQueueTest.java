package test.zylqueue;

import junit.framework.TestCase;


/**
 * Created by 竹韵澜 on 2017/10/18.
 */

public class LinkedQueueTest extends TestCase {

    LinkedQueue<Integer> linkedQueue = new LinkedQueue<>();
    public void testEnqueue(){
        assertEquals(true, linkedQueue.isEmpty());
        linkedQueue.enqueue(1);
        linkedQueue.enqueue(2);
        assertEquals(false,linkedQueue.isEmpty());
    }

    public void testDequeue(){
        linkedQueue.enqueue(1);
        linkedQueue.enqueue(2);
        assertEquals("1",linkedQueue.dequeue().toString());
    }

    public void testSize(){
        linkedQueue.enqueue(1);
        linkedQueue.enqueue(2);
        linkedQueue.enqueue(3);
        assertEquals(3,linkedQueue.size());
    }

    public void testFirst(){
        linkedQueue.enqueue(1);
        linkedQueue.enqueue(3);
        linkedQueue.enqueue(5);
        assertEquals("1",linkedQueue.first().toString());
    }
}
