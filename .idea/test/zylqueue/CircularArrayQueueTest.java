package test.zylqueue;


import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/10/18.
 */

public class CircularArrayQueueTest extends TestCase {
    CircularArrayQueue<Integer> circularArrayQueue =  new CircularArrayQueue<>();
    public void testEnqueue(){
        assertEquals(true,circularArrayQueue.isEmpty());
        circularArrayQueue.enqueue(1);
        circularArrayQueue.enqueue(2);
        assertEquals(false,circularArrayQueue.isEmpty());
    }

    public void testtDequeue(){
        circularArrayQueue.enqueue(1);
        circularArrayQueue.enqueue(2);
        circularArrayQueue.enqueue(3);
        assertEquals("1",circularArrayQueue.dequeue().toString());
    }

    public void testSize(){
        circularArrayQueue.enqueue(1);
        circularArrayQueue.enqueue(3);
        circularArrayQueue.enqueue(5);
        assertEquals(3,circularArrayQueue.size());
    }

    public void testFirst1(){
        circularArrayQueue.enqueue(1);
        circularArrayQueue.enqueue(2);
        circularArrayQueue.enqueue(3);
        assertEquals("1",circularArrayQueue.first().toString());
    }

    public void testFirst2(){
        circularArrayQueue.enqueue(3);
        circularArrayQueue.enqueue(2);
        circularArrayQueue.enqueue(1);
        assertEquals("3",circularArrayQueue.first().toString());
    }
}
