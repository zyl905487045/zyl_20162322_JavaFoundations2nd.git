package zyl22;


import cn.edu.besti.cs1623.zyl22.OtherSearch.InsertionSearch;
import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */

public class InsertionSearchTest extends TestCase {
    InsertionSearch insertion = new InsertionSearch();
    int[] data =  { 1, 5, 15, 22, 25, 31, 39, 42, 47, 49, 59, 68, 88 };
    public void testInsertionSearch() throws Exception {
        assertEquals(7,insertion.InsertionSearch(data,39));
    }
}
