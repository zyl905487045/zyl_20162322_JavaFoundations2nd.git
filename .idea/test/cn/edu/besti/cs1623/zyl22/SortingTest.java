package zyl22;

import SearchAndSort.Sorting;
import junit.framework.TestCase;


/**
 * Created by 竹韵澜 on 2017/11/6.
 */

public class SortingTest extends TestCase {
    SearchAndSort.Sorting sort = new Sorting();
    Comparable comparable[] = {90,8,7,56,123,235,9,1,653,2322};

    public void testSelectionSort() throws Exception {
        sort.selectionSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sorting);
    }

    public void testInsertionSort() throws Exception {
        sort.insertionSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sorting);

    }

    public void testBubbleSort() throws Exception {
        sort.bubbleSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sorting);

    }

    public void testQuickSort() throws Exception {
        sort.quickSort(comparable,0,9);
        for (Comparable sorting : comparable)
            System.out.println(sorting);
    }

    public void testMergeSort() throws Exception {
        sort.mergeSort(comparable,0,9);
        for (Comparable sorting : comparable)
            System.out.println(sorting);
    }

}
