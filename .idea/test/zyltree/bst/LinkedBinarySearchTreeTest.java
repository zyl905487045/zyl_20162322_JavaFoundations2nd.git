package test.zyltree.bst;


import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/10/27.
 */

public class LinkedBinarySearchTreeTest  extends TestCase {
    BSTNode<Integer> bstNode = new BSTNode<>(1);
    LinkedBinarySearchTree<Integer> linkedBinarySearchTree = new LinkedBinarySearchTree<Integer>(bstNode);

    public void testFindMin() {
   linkedBinarySearchTree.add(1);
   linkedBinarySearchTree.add(3);
   linkedBinarySearchTree.add(5);
   linkedBinarySearchTree.add(7);
   linkedBinarySearchTree.add(9);
    assertEquals("1",linkedBinarySearchTree.findMin()+"");
    }


    public void testfindMax(){
        linkedBinarySearchTree.add(1);
        linkedBinarySearchTree.add(3);
        linkedBinarySearchTree.add(5);
        linkedBinarySearchTree.add(7);
        linkedBinarySearchTree.add(9);
        assertEquals("9",linkedBinarySearchTree.findMax()+"");
    }


}
