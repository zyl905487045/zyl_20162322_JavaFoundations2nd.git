package test.zyltree;

import junit.framework.TestCase;


/**
 * Created by 竹韵澜 on 2017/10/23.
 */
public class LinkedBinaryTreeTest2 extends TestCase {

    LinkedBinaryTree<Integer> linkedBinaryTree1 = new LinkedBinaryTree<Integer>(1);
    LinkedBinaryTree<Integer> linkedBinaryTree2 = new LinkedBinaryTree<Integer>(2);
    LinkedBinaryTree<Integer> linkedBinaryTree3 = new LinkedBinaryTree<Integer>(3,linkedBinaryTree1,linkedBinaryTree2);

    public void  testGetRight(){
        assertEquals("2",linkedBinaryTree3.getRight().getRootElement().toString());
    }

    public void  testContains(){
        assertEquals(true,linkedBinaryTree3.contains(1));
        assertEquals(false,linkedBinaryTree3.contains(4));
    }

    public void  testPreorder(){
        assertEquals(1,linkedBinaryTree3.preorder().toString().indexOf("3"));
        assertEquals(4,linkedBinaryTree3.preorder().toString().indexOf("1"));
        assertEquals(7,linkedBinaryTree3.preorder().toString().indexOf("2"));
    }

    public void  testPostorder(){
        assertEquals(1,linkedBinaryTree3.postorder().toString().indexOf("1"));
        assertEquals(4,linkedBinaryTree3.postorder().toString().indexOf("2"));
        assertEquals(7,linkedBinaryTree3.postorder().toString().indexOf("3"));
    }
}