package test.zylheap;


import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/11/3.
 */

public class LinkedMaxHeapTest  extends TestCase {
    LinkedMaxHeap<Integer> linkedMaxHeap = new LinkedMaxHeap<>();

    public void testAdd() throws Exception {
        linkedMaxHeap.add(1);
        linkedMaxHeap.add(4);
        linkedMaxHeap.add(9);
        linkedMaxHeap.add(22);
    }


    public void testGetMax() throws Exception {
        linkedMaxHeap.add(44);
        linkedMaxHeap.add(56);
        linkedMaxHeap.add(33);
        linkedMaxHeap.add(68);
        assertEquals("68",linkedMaxHeap.getMax().toString());
    }

    public void testRemoveMax() throws Exception {
        linkedMaxHeap.add(1);
        linkedMaxHeap.add(4);
        linkedMaxHeap.add(9);
        linkedMaxHeap.add(22);
        linkedMaxHeap.add(44);
        linkedMaxHeap.add(56);
        linkedMaxHeap.add(33);
        linkedMaxHeap.add(68);
        linkedMaxHeap.removeMax();
        assertEquals("56",linkedMaxHeap.getMax().toString());
    }

}
