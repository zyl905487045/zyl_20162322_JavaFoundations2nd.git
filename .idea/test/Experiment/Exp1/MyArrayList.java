package Experiment.Exp1;

import java.util.Arrays;

/**
 * Created by 竹韵澜 on 2017/9/26.
 */
public class MyArrayList<T> {
    private int DEFAULT_SIZE = 16;// 线性表的默认长度空间
    private int capacity;// 线性表的实际分配数组长度
    private int size = 0;// 线性表的当前元素个数及线性表的长度
    private Object[] element;// 数据元素封装一个数组
    // 无参构造线性表
    public MyArrayList() {
        this.element = new Object[DEFAULT_SIZE];// 初始化列表的空间
        this.capacity = DEFAULT_SIZE;// 实际分配数组长度
    }
    // 初始化含有一个元素的线性表
    public MyArrayList(T elem) {
        this();// 调用空参数构造函数
        this.element[size++] = elem;
    }
    // 指定长度并初始化一个元素创建线性表
    public MyArrayList(T elem, int size) {
        this.capacity = 1;// 初始化
        // 扩充数组空间使得capicity的size且是2的n次方
        while (this.capacity < size) {
            this.capacity <<= 1;
        }
        this.element = new Object[this.capacity];
        this.element[size++] = elem;
    }
    // 获得线性表的长度
    public int length() {
        return this.size;
    }
    // 获取线性表的索引为i处的元素(i介于0~size-1）
    @SuppressWarnings("unchecked")
    public T getelem(int i) {
        if (i < 0 || i > size - 1) {// 检测是否越界
            throw new IndexOutOfBoundsException("线性表的索引越界:" + i);
        }
        return (T) this.element[i];
    }
    // 查找元素在线性表中的索引
    public int findindex(T elem) {
        for (int i = 0; i < size; i++) {
            if (this.element[i].equals(elem))
                return i;// 找到返回对应的索引
        }
        return -1;// 若没有找到返回-1
    }
    // 扩充线性表的容量
    private void ensureCapicty(int currentCapicty) {
        if (currentCapicty > this.capacity) {// 若实际的所需容量大于实际的容量则扩充使得实际容量大于所需容量且是2的次方
            while (this.capacity < currentCapicty) {
                this.capacity <<= 1;
            }
            // 将原来的元素拷贝到新的位置上
            this.element = Arrays.copyOf(this.element, this.capacity);
        }
    }
    // 插入一个元素到线性表的第i个索引处
    public void insert(T elem, int i) {
        if (i < 0 || i > size) {// 插入位置非法
            throw new IndexOutOfBoundsException("插入元素线性表索引位置越界:" + i);
        }
        // 是否需要扩充容量
        this.ensureCapicty(size + 1);
        // 插入元素第i个位置空出来从i位置开始所有元素后移一个位置
        System.arraycopy(this.element, i, this.element, i + 1, size - i);
        // 将元素插入到指定位置
        this.element[i] = elem;
        // 当前容量增加1
        this.size++;
    }
    // 在线性表末尾插入元素
    public void add(T elem) {
        this.insert(elem, this.size);
    }
    // 删除线性表中第i个元素并返回该处的值
    public T delete(int i) {
        if (i < 0 || i > size - 1) {// 检测删除位置对不对
            throw new IndexOutOfBoundsException("删除位置索引越界:" + i);
        }
        // 获得i处的元素值
        @SuppressWarnings("unchecked")
        T del = (T) this.element[i];
        // 删除元素后从i+1位置开始元素要前移
        int moved = this.size - i - 1;// 需要移动元素的个数
        if (moved > 0) {
            System.arraycopy(this.element, i + 1, this.element, i, moved);
        }
        // 清空最后一个元素
        this.element[--size] = null;
        return del;
    }
    // 移除线性表中最后一个元素
    public T remove() {
        return this.delete(size - 1);
    }
    // 判断线性表是否为空
    public boolean empty() {
        return this.size == 0;
    }
    // 清空线性表
    public void clear() {
        // 将所有元素赋值为null
        Arrays.fill(this.element, null);
        this.size = 0;
    }
    // 覆写toString
    public String toString() {
        if (this.size == 0) {
            return "[]";
        } else {
            // 返回字符串的字符串表示
            StringBuffer sb = new StringBuffer("[");
            for (int i = 0; i < this.size - 1; i++) {
                sb.append(this.element[i].toString() + ",");
            }
            sb.append(this.element[this.size - 1].toString() + "]");
            return sb.toString();
        }
    }
}
