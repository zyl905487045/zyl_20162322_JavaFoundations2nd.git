package Experiment.Exp1;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 竹韵澜 on 2017/9/26.
 */
public class MergeListTest extends TestCase {
    List bList = new ArrayList();
    List aList = new ArrayList();


    public void testMergeSortedList() throws Exception {

        bList.add(1);
        bList.add(9);
        bList.add(70);
        bList.add(80);
        bList.add(90);
        aList.add(1);
        aList.add(3);
        aList.add(5);
        aList.add(7);
        aList.add(90);

        List mergeList = MergeList.mergeSortedList(aList,bList);


        assertEquals("",1,mergeList.get(0));
        assertEquals("",1,mergeList.get(1));
        assertEquals("",3,mergeList.get(2));
        assertEquals("",5,mergeList.get(3));
        assertEquals("",7,mergeList.get(4));
        assertEquals("",9,mergeList.get(5));
        assertEquals("",70,mergeList.get(6));
        assertEquals("",80,mergeList.get(7));
        assertEquals("",90,mergeList.get(8));
        assertEquals("",90,mergeList.get(9));
    }

}