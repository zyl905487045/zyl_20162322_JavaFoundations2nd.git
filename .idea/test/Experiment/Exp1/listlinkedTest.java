package Experiment.Exp1;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.LinkedList;

/**
 * Created by 竹韵澜 on 2017/9/25.
 */
public class listlinkedTest extends TestCase {
    LinkedList linkedList = new LinkedList();

    @Test
    public void testAdd(){
        assertEquals(0,linkedList.size());
        linkedList.add("Hello");
        assertEquals(1,linkedList.size());
    }

    @Test
    public void testIsEmpty(){
        linkedList.add("Hello");
        assertEquals(false,linkedList.isEmpty());
        linkedList.clear();
        assertEquals(true,linkedList.isEmpty());
    }

    @Test
    public void testClear(){
        linkedList.add("1");
        linkedList.add("2");
        assertEquals(2,linkedList.size());
        linkedList.clear();
        assertEquals(0,linkedList.size());
    }
}