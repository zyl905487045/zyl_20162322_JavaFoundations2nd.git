package Experiment.Exp1;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by 竹韵澜 on 2017/9/25.
 */
public class listarrayTest extends TestCase {

    ArrayList arrayList = new ArrayList();


    @Test
    public void testAdd(){
        assertEquals(0,arrayList.size());
        arrayList.add("Hello");
        assertEquals(1,arrayList.size());
    }

    @Test
    public void testIsEmpty(){
        arrayList.add("Hello");
        assertEquals(false,arrayList.isEmpty());
        arrayList.clear();
        assertEquals(true,arrayList.isEmpty());
    }

    @Test
    public void testClear(){
        arrayList.add("1");
        arrayList.add("2");
        assertEquals(2,arrayList.size());
        arrayList.clear();
        assertEquals(0,arrayList.size());
    }
}