package Experiment.Exp1;

import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/9/26.
 */
public class MyArrayListTest extends TestCase{
    MyArrayList arrayList = new MyArrayList<Integer>();

    public void testLength() throws Exception {
        arrayList.add(22);
        arrayList.add(20162322);
        assertEquals(2,arrayList.length());
    }

    public void testInsert() throws Exception {
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(5);
        arrayList.add(6);
        assertEquals(5,arrayList.length());
        arrayList.insert(4,3);
        assertEquals(6,arrayList.length());
    }

    public void testAdd() throws Exception {
        assertEquals(true,arrayList.empty());
        arrayList.add(20162322);
        assertEquals(false,arrayList.empty());
    }

    public void testDelete() throws Exception {
        arrayList.add(1);
        assertEquals(1,arrayList.length());
        arrayList.delete(0);
        assertEquals(0,arrayList.length());
    }
}
