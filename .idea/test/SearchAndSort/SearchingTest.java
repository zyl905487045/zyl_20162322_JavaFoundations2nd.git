package SearchAndSort;

import junit.framework.TestCase;


/**
 * Created by 竹韵澜 on 2017/11/6.
 */

public class SearchingTest extends TestCase {
    Comparable  array[] = {1, 3, 7, 100 , 2322, -99, 10000};
    Searching linearsearch = new Searching();
    Searching binarySearch = new Searching();
    public void testLinearSearch1() {
        assertEquals("1",linearsearch.LinearSearch(array,1).toString());
    }

    public void testLinearSearch2() {
        assertEquals("10000",linearsearch.LinearSearch(array,10000).toString());
    }

    public void testLinearSearch3() {
        assertEquals(null,linearsearch.LinearSearch(array,9));
    }

    public void testLinearSearch4() {
        assertEquals(null,linearsearch.LinearSearch(array,-2322));
    }

    public void testLinearSearch5() {
        assertEquals("2322",linearsearch.LinearSearch(array,2322).toString());
    }


    public void testBinarySearch1() {
        assertEquals("1",binarySearch.BinarySearch(array,1).toString());
    }

    public void testBinarySearch2(){
        assertEquals(10000,binarySearch.BinarySearch(array,10000));
    }


    public void testBinarySearch3() {
        assertEquals(null,binarySearch.BinarySearch(array,9));
    }

    public void testBinarySearch4() {
        assertEquals(null,binarySearch.BinarySearch(array,-2322));
    }

    public void testBinarySearch5(){
        assertEquals(null,binarySearch.BinarySearch(array,-2322));
    }


}
