package zylstack;

import junit.framework.TestCase;

/**
 * Created by 竹韵澜 on 2017/10/9.
 */
public class ArrayStackTest extends TestCase {

    ArrayStack arrayStack = new ArrayStack();

    public void testPush() throws Exception {
        assertEquals(true, arrayStack.isEmpty());
        arrayStack.push("20162322");
        assertEquals(false,arrayStack.isEmpty());
    }

    public void testPop() throws Exception {
        assertEquals(true, arrayStack.isEmpty());
        arrayStack.push("20162322");
        assertEquals(false,arrayStack.isEmpty());
        arrayStack.pop();
        assertEquals(false,arrayStack.isEmpty());

    }

    public void testPeek() throws Exception {
        arrayStack.push("20162322");
        assertEquals("20162322",arrayStack.peek());
    }

    public void testIsEmpty() throws Exception {
        assertEquals(true, arrayStack.isEmpty());
        arrayStack.push("20162322");
        assertEquals(false,arrayStack.isEmpty());
    }

    public void testSize() throws Exception {
        arrayStack.push("20162322");
        arrayStack.push("20162322");
        arrayStack.push("20162322");
        arrayStack.push("20162322");
        arrayStack.push("20162322");
        arrayStack.push("20162322");
        arrayStack.push("20162322");
        assertEquals(7,arrayStack.size());
    }

}