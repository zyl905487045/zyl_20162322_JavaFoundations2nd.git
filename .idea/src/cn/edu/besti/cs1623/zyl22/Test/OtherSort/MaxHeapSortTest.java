package zyl22.Test.OtherSort;

import cn.edu.besti.cs1623.zyl22.OtherSort.MaxHeapSort;
import zylheap.MaxHeap;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class MaxHeapSortTest {
    public static void main(String[] args) {
        int[] array=new int[]{1,2,3,4,7,8,9,10,14,16};
        MaxHeapSort heap=new MaxHeapSort(array);
        System.out.println("执行最大堆化前堆的结构：");
        for(int i=1;i<array.length;i=i*2) {
            for(int k=i-1;k<2*(i)-1&&k<array.length;k++) {
                System.out.print(array[k]+" ");
            }
            System.out.println();
        }

        heap.BuildMaxHeap();
        System.out.println("执行最大堆化后堆的结构：");
        for(int i=1;i<array.length;i=i*2) {
            for(int k=i-1;k<2*(i)-1&&k<array.length;k++) {
                System.out.print(array[k]+" ");
            }
            System.out.println();
        }

        heap.HeapSort();
        System.out.println("执行堆排序后数组的内容");
        for(int i=0;i<array.length;i++) {
            System.out.print(array[i]+" ");
        }
    }

    private static void printHeapTree(int[] array) {
        for(int i=1;i<array.length;i=i*2) {
            for(int k=i-1;k<2*(i)-1&&k<array.length;k++) {
                System.out.print(array[k]+" ");
            }
            System.out.println();
        }
    }
    private static void printHeap(int[] array) {
        for(int i=0;i<array.length;i++) {
            System.out.print(array[i]+" ");
        }
    }


}
