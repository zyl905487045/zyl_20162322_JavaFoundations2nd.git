package zyl22.Test;


import SearchAndSort.Searching;

/**
 * Created by 竹韵澜 on 2017/10/9.
 */
public class TestSearch {
    public static void main(String[] args) {
       Searching linearsearch = new Searching();
       Searching binarySearch = new Searching();

       Comparable array[] = {3,8,12,34,54,84,91,110,2322};

        linearsearch.LinearSearch(array,45);
        System.out.println(linearsearch.LinearSearch(array,45));

        linearsearch.LinearSearch(array,54);
        System.out.println(linearsearch.LinearSearch(array,54));

        binarySearch.BinarySearch(array,45);
        System.out.println( binarySearch.BinarySearch(array,45));

        binarySearch.BinarySearch(array,54);
        System.out.println(binarySearch.BinarySearch(array,54));

    }
}
