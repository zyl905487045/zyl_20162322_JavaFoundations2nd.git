package zyl22.Test;

import SearchAndSort.Sorting;

/**
 * Created by 竹韵澜 on 2017/10/9.
 */
public class TestSort {
    public static void main(String[] args) {
        Sorting sort = new Sorting();

        Comparable comparable[] = {90,8,7,56,123,235,9,1,653,2322};

        sort.selectionSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sorting);

        sort.insertionSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sorting);

        sort.bubbleSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sorting);

        sort.quickSort(comparable,0,9);
        for (Comparable sorting : comparable)
            System.out.println(sorting);

        sort.mergeSort(comparable,0,9);
        for (Comparable sorting : comparable)
            System.out.println(sorting);
    }
}

