package zyl22.Test;

import cn.edu.besti.cs1623.zyl22.OtherSearch.InsertionSearch;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class InsertionSearchTest {
    public static void main(String[] args) {
        InsertionSearch insertion = new InsertionSearch();
        int[] data = { 1, 5, 15, 22, 25, 31, 39, 42, 47, 49, 59, 68, 88 };
        int search = 39;
        int position = insertion.InsertionSearch(data,search) ;
        if (position != -1)
            System.out.println("查找成功，值" + search + "的元素位置为：" + position);

        int search2 = 45;
        int position2 = insertion.InsertionSearch(data,search2);
        if (position2 == -1)
            System.out.println("查找失败，不存在值" + search2);
    }
}
