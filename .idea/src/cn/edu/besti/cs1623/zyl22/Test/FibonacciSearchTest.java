package zyl22.Test;

import cn.edu.besti.cs1623.zyl22.OtherSearch.FibonacciSearch;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class FibonacciSearchTest {
    public static void main(String[] args) {
        FibonacciSearch fibon = new FibonacciSearch();
        int[] f = fibon.fibonacci();
        for (int i : f) {
            System.out.print(i + " ");
        }
        System.out.println();

        Comparable[] data = { 1, 5, 15, 22, 25, 31, 39, 42, 47, 49, 59, 68, 88 };

        Comparable search = 39;
        int position = (int) fibon.fibonacciSearch(data, (int)search) + 1;
        System.out.println("值" + search + "的元素位置为：" + position);
    }
}
