package zyl22.Test.OtherSort;

import cn.edu.besti.cs1623.zyl22.OtherSort.ShellSort;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class ShellSortTest {
    public static void main(String[] args) {
        ShellSort shellSort = new ShellSort();
        int[] data = new int[] { 5, 3, 6, 2, 1, 9, 4, 8, 7 };
        for (int k = 0; k < data.length; k++) {
            System.out.print(data[k] + "\t");
        }
        System.out.println();
        shellSort.shellSort(data);
        for (int k = 0; k < data.length; k++) {
            System.out.print(data[k] + "\t");
        }
        System.out.println();

    }
}
