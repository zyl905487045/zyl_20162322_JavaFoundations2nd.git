package zyl22.Test.OtherSort;

import cn.edu.besti.cs1623.zyl22.OtherSort.BucketSort;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class BucketSortTest {
    public static void main(String[] args) {
        BucketSort bucketSort = new BucketSort();
        int[] x = { 99, 65, 24, 47, 50, 88,33, 66, 67, 31, 18 };
        int[] sorted = bucketSort.BucketSort(x, 99);
        for (int i = 0; i < sorted.length; i++) {
            if (sorted[i] > 0)
                System.out.println(sorted[i]);
        }
    }
}
