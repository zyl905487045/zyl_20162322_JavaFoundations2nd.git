package zyl22.OtherSort;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class ShellSort {
    public static void shellSort(int[] data) {
        // 计算出最大的h值
        int h = 1;
        while (h <= data.length / 3) {
            h = h * 3 + 1;
        }
        while (h > 0) {
            for (int i = h; i < data.length; i += h) {
                if (data[i] < data[i - h]) {
                    int tmp = data[i];
                    int j = i - h;
                    while (j >= 0 && data[j] > tmp) {
                        data[j + h] = data[j];
                        j -= h;
                    }
                    data[j + h] = tmp;

                    for (int k = 0; k < data.length; k++) {
                        System.out.print(data[k] + "\t");
                    }
                    System.out.println();
                }
            }
            // 计算出下一个h值
            h = (h - 1) / 3;
        }
    }

}
