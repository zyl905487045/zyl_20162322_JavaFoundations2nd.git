package zyl22.OtherSort;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class BucketSort {
    public static int[] BucketSort(int[] nums, int maxNum){
        int[] sorted = new int[maxNum+1];

        for(int i=0; i<nums.length; i++){
            sorted[nums[i]] = nums[i];//把数据放到对应索引的位置
        }
        return sorted;
    }
}
