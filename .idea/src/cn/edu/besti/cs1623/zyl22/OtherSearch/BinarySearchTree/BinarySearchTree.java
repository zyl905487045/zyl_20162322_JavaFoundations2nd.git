package zyl22.OtherSearch.BinarySearchTree;

import zyltree.BinaryTree;

/**
 * Created by 竹韵澜 on 2017/10/26.
 */
public interface BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T> {
    //  将指定的元素添加到树中。
    public void add(T element);

    //  查找并返回与指定目标匹配的树中的元素。 覆盖BinaryTree的find方法。
    public T find(T target);

    //  返回二叉搜索树中的最小值。
    public T findMin();

    //  返回二叉搜索树中的最大值。
    public T findMax();

    //  从树中删除并返回指定的元素。
    public T remove(T target);
}
