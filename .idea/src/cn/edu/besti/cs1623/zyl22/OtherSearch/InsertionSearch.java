package zyl22.OtherSearch;

/**
 * Created by 竹韵澜 on 2017/11/6.
 */
public class InsertionSearch {

    public static int InsertionSearch(int [] data, int target){
        int low, high, mid;
        low = 0;
        high = data.length - 1;
        while(low <= high){
				/* 插值查找的计算公式 */
            mid = low + (high-low) * (target-data[low])/(data[high]-data[low]);
            if (target < data[mid]){
                high = mid - 1;
            }
            else if (target > data[mid]){
                low = mid + 1;
            }
            else
                return mid + 1;
        }
        return -1;
    }

}
