package SearchAndSort;

/**
 * Created by 竹韵澜 on 2017/10/9.
 */
public class SortTest {
    public static void main(String[] args) {
        Sorting sort = new Sorting();

        Comparable comparable[] = {90,8,7,56,123,235,9,1,653,2322};

        sort.quickSort(comparable,0,9);
        for (Comparable sorting : comparable)
            System.out.println(sort);

        sort.selectionSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sort);

        sort.insertionSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sort);

        sort.bubbleSort(comparable);
        for (Comparable sorting : comparable)
            System.out.println(sort);

        sort.mergeSort(comparable,0,9);
        for (Comparable sorting : comparable)
            System.out.println(sort);
    }
}

