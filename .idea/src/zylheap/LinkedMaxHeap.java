package zylheap;

import exceptions.EmptyCollectionException;
import zyltree.LinkedBinaryTree;

/**
 * Created by 竹韵澜 on 2017/11/2.
 */
public class LinkedMaxHeap<T extends Comparable<T>> extends LinkedBinaryTree<T> implements MaxHeap<T> {
    private HeapNode<T> last;

    public LinkedMaxHeap(){
        super();
        last = null;
    }

    public LinkedMaxHeap(T element){
        root = new HeapNode<T>(element);
        last = (HeapNode<T>)root;
    }
    @Override
    public void add(T element) {
        HeapNode<T> node = new HeapNode<T>(element);
        HeapNode<T> newParent = null;

        if (root == null)
            root = node;
        else {
            newParent = ((HeapNode<T>)root).getParentAdd(last);

            if (newParent.left == null)
                newParent.setLeft(node);
            else
                newParent.setRight(node);
        }

        node.setParent(newParent);
        last = node;

        ((HeapNode<T>)root).heapifyAdd(last);
    }

    @Override
    public T getMax() {

        return  getRootElement();
    }

    @Override
    public T removeMax() {
        if (root==null)
            throw new EmptyCollectionException("Remove max operation " +
                    "failed. Tree is empty.");

        T maxElement = root.getElement();

        if (root.count() == 1)
            root = last = null;
        else {
            HeapNode<T> newlast = ((HeapNode<T>)root).getNewLastNode(last);
            if (last.parent.left == last)
                last.parent.left = null;
            else
                last.parent.right = null;

            root.setElement(last.getElement());
            last=newlast;
            ((HeapNode<T>)root).heapifyRemove((HeapNode<T>)root);
        }
        return maxElement;
    }
}
