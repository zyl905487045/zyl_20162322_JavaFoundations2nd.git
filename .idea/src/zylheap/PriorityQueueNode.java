package zylheap;

/**
 * Created by 竹韵澜 on 2017/11/2.
 */
public class PriorityQueueNode<T> implements Comparable<PriorityQueueNode<T>> {
    private static int nextorder = 0;
    private int priority;
    private int order;
    private T element;

    public PriorityQueueNode (T obj, int prio){
        element = obj;
        priority = prio;
        order = nextorder;
        nextorder++;
    }

    public T getElement() {
        return element;
    }

    public int getPriority() {
        return priority;
    }

    public int getOrder() {
        return order;
    }

    public String toString(){
        String temp = (element.toString() + priority + order);
        return temp;
    }

    @Override
    public int compareTo(PriorityQueueNode obj) {
        int result;

        if (priority < getPriority())
            result = -1;
        else if (priority > obj.getPriority())
            result = 1;
        else if (order > obj.getOrder())
            result = -1;
        else
            result = 1;

        return result;
    }

}
