package zylheap;

/**
 * Created by 竹韵澜 on 2017/11/5.
 */
public class PriorityQueue<T> extends LinkedMaxHeap<PriorityQueueNode<T>> {
    public PriorityQueue(){
        super();
    }

    public void addElement (T object, int priority){
        PriorityQueueNode<T> node = new PriorityQueueNode<T>(object,priority);
        super.add(node);
    }

    public T removeNext(){
        PriorityQueueNode<T> temp = (PriorityQueueNode<T>)super.removeMax();
        return temp.getElement();
    }
}
