package zylheap;

import zyltree.BTNode;

/**
 * Created by 竹韵澜 on 2017/11/2.
 */
public class HeapNode<T extends Comparable<T>> extends BTNode<T> {
    HeapNode<T> parent;

    public HeapNode(T element) {
        super(element);
        parent=null;
    }

    public HeapNode<T> getParent() {
        return parent;
    }

    public void setParent(HeapNode<T> parent) {
        this.parent = parent;
    }

    public HeapNode<T> getParentAdd (HeapNode<T> last){
        HeapNode<T> result = last;
        while ((result.parent != null) && (result.parent.left != result))
            result=result.parent;

        if (result.parent != null)
            if (result.parent.right == null)
                result=result.parent;
            else{
                result=(HeapNode<T>)result.parent.right;
                while (result.left != null)
                    result=(HeapNode<T>)result.left;
            }
        else
            while (result.left != null)
                result=(HeapNode<T>)result.left;

        return result;
    }

    public void heapifyAdd (HeapNode<T> last){
        T temp;
        HeapNode<T> current = last;

        while ((current.parent != null)&&
                (current.element).compareTo(current.parent.element)>0){
            temp = current.element;
            current.element = current.parent.element;
            current.parent.element = temp;
            current = current.parent;
        }
    }

    public HeapNode<T> getNewLastNode(HeapNode<T> last){
        HeapNode<T> result = last;
        while ((result.parent != null) && (result.parent.left == result))
            result = result.parent;

        if (result.parent != null)
            result = (HeapNode<T>) result.parent.left;

        while (result.right != null)
            result = (HeapNode<T>) result.right;

        return result;
    }

    public void heapifyRemove(HeapNode<T> root){
        T temp;
        HeapNode<T> current = root;
        HeapNode<T> next = largerChild(current);

        while (next != null && next.element.compareTo(current.element) > 0){
            temp=current.element;
            current.element=next.element;
            next.element=temp;

            current=next;
            next=largerChild(current);
        }
    }

    public HeapNode<T> largerChild (HeapNode<T> node){
        HeapNode<T> larger = null;

        if (node.left == null && node.right == null)
            larger=null;
        else if (node.left == null)
            larger=(HeapNode<T>)node.right;
        else if (node.right == null)
            larger=(HeapNode<T>)node.left;
        else if (((HeapNode<T>)node.left).element.compareTo(((HeapNode<T>)node.right).element) > 0)
            larger=(HeapNode<T>)node.left;
        else
            larger=(HeapNode<T>)node.right;

        return larger;
    }
}
