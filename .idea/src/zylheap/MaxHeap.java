package zylheap;

import zyltree.BinaryTree;

/**
 * Created by 竹韵澜 on 2017/11/2.
 */
public interface MaxHeap<T extends Comparable<T>> extends BinaryTree<T> {
    public void add(T obj);
    public T getMax();
    public T removeMax();
}
