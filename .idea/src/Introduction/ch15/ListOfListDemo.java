package Introduction.ch15;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class ListOfListDemo {
    public static void main(String[] args) {
        List<String> listOfStrings = new ArrayList<>();
        listOfStrings.add("Hello again");
        List<List<String>> listOfLists = new ArrayList<>();
        listOfLists.add(listOfStrings);
        String s = listOfLists.get(0).get(0);
        System.out.println(s); // Prints "Hello again"
    }
}
