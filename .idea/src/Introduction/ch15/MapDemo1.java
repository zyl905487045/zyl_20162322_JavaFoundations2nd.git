package Introduction.ch15;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class MapDemo1 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Key1","Value1");
        map.put("Key2","Value2");
        String value1 = map.get("Map1");
    }
}
