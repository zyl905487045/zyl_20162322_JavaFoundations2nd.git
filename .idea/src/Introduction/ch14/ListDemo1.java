package Introduction.ch14;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class ListDemo1 {
    public static void main(String[] args) {
        List mylist = new ArrayList();
        String s1 = "Hello";
        String s2 = "Hello";
        mylist.add(100);
        mylist.add(s1);
        mylist.add(s2);
        mylist.add(s1);
        mylist.add(1);
        mylist.add(2,"World");
        mylist.add(3,"Yes");
        mylist.add(null);
        System.out.println("Size: " + mylist.size());
        for(Object object : mylist){
            System.out.println(object);
        }
    }
}
