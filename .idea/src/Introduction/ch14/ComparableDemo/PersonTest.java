package Introduction.ch14.ComparableDemo;

import java.util.Arrays;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class PersonTest {
    public static void main(String[] args) {
        Person[] people = new Person[4];
        people[0] = new Person();
        people[0].setFirstname("A");
        people[0].setLastname("d");
        people[0].setAge(56);

        people[1] = new Person();
        people[1].setFirstname("B");
        people[1].setLastname("c");
        people[1].setAge(8);

        people[2] = new Person();
        people[2].setFirstname("C");
        people[2].setLastname("b");
        people[2].setAge(16);

        people[3] = new Person();
        people[3].setFirstname("D");
        people[3].setLastname("a");
        people[3].setAge(69);

        System.out.println("Natural Order");
        for(int i = 0;i<4 ; i++){
            Person person = people[i];
            String lastname = person.getLastname();
            String firstname = person.getFirstname();
            int age = person.getAge();
            System.out.println(lastname + ", " + firstname +
            ". Age:" + age);
        }

        Arrays.sort(people, new LastNameComparator());
        System.out.println("Sorted by last name");
        for(int i = 0;i<4 ; i++){
            Person person = people[i];
            String lastname = person.getLastname();
            String firstname = person.getFirstname();
            int age = person.getAge();
            System.out.println(lastname + ", " + firstname +
                    ". Age:" + age);
        }

        Arrays.sort(people, new FirstNameComparator());
        System.out.println("Sorted by first name");
        for(int i = 0;i<4 ; i++){
            Person person = people[i];
            String lastname = person.getLastname();
            String firstname = person.getFirstname();
            int age = person.getAge();
            System.out.println(lastname + ", " + firstname +
                    ". Age:" + age);
        }

        Arrays.sort(people);
        System.out.println("Sorted by age");
        for(int i = 0;i<4 ; i++){
            Person person = people[i];
            String lastname = person.getLastname();
            String firstname = person.getFirstname();
            int age = person.getAge();
            System.out.println(lastname + ", " + firstname +
                    ". Age:" + age);
        }
    }
}
