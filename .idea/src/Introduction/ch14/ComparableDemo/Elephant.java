package Introduction.ch14.ComparableDemo;


import java.lang.*;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class Elephant implements java.lang.Comparable {
    public float weight;
    public int age;
    public float tusklength;

    @Override
    public int compareTo(Object obj) {
        Elephant anotherElephant = (Elephant) obj;
        if(this.weight > anotherElephant.weight){
            return 1;
        } else if(this.weight < anotherElephant.weight) {
            return -1;
        }else {
            // both elephants have the same weight, now
            // compare their age
            return (this.age - anotherElephant.age);
        }
    }
}
