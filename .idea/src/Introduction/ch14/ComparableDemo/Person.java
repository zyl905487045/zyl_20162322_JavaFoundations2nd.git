package Introduction.ch14.ComparableDemo;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class Person implements Comparable{
    private String firstname;
    private String lastname;
    private int age;
    public String getFirstname(){
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname(){
        return lastname;
    }
    public void setLastname(String lastname){
        this.lastname = lastname;
    }
    public int getAge(){
        return age;
    }
    public void  setAge(int age){
        this.age = age;
    }
    @Override
    public int compareTo(Object anotherPerson)
        throws ClassCastException {
            if (!(anotherPerson instanceof Person)){
                throw new ClassCastException("A Person object expected.");
            }
            int anotherPersonAge = ((Person) anotherPerson).getAge();
            return this.age - anotherPersonAge;
    }

}
