package Introduction.ch14.ComparableDemo;

import java.util.Comparator;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class LastNameComparator implements Comparator {
    @Override
    public int compare(Object person, Object anotherperson) {
        String lastname1 =
                ((Person) person).getLastname().toUpperCase();
        String firstname1 =
                ((Person) person).getFirstname().toUpperCase();
        String lastname2 =
                ((Person) anotherperson).getLastname().toUpperCase();
        String firstname2 =
                ((Person) anotherperson).getFirstname().toUpperCase();
        if (lastname1.equals(lastname2)){
            return firstname1.compareTo(firstname2);
        }else {
            return lastname1.compareTo(lastname2);
        }
    }
}
