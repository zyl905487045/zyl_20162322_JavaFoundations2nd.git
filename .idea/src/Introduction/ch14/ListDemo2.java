package Introduction.ch14;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by 竹韵澜 on 2017/9/16.
 */
public class ListDemo2 {
    public static void main(String[] args) {
        List numbers = Arrays.asList(9,4,-9,100);
        Collections.sort(numbers);
        for (Object i : numbers){
            System.out.println(i);
        }
    }
}
