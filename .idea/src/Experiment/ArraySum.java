package Experiment;

/**
 * Created by 竹韵澜 on 2017/9/24.
 */
public class ArraySum {
    public int  sumArray(int array[][]){
        int sum = 0;
        for (int i = 0; i < array.length ; i++) {

            for (int j = 0; j < array[i].length; j++) {
                sum = sum + array[i][j];
            }
        }
        return sum;
    }
}
