package Experiment.Bag; /**
 * Created by 竹韵澜 on 2017/9/22.
 */
     /**
     An interface that describes the operations of a bag of objects.
     */
     public interface BagInterface<T> {

         /** Sees whether this bag is empty.
         @return True if the bag is empty, or false if not. */
         public boolean isEmpty();

         /** Adds a new entry to this bag.
          @param newEntry The object to be added as a new entry.
          @return True if the addition is successful, or false if not. */
         public boolean add(T newEntry);


         /** Removes one occurrence of a given entry from this bag, if possible.
           @param anEntry The entry to be removed.
           @return True if the removal was successful, or false if not. */
         public boolean remove (T anEntry);




 } // end BagInterf
