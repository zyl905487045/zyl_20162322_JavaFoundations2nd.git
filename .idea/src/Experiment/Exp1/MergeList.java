package Experiment.Exp1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 竹韵澜 on 2017/9/26.
 */
public class MergeList extends ArrayList{
    protected static List<? extends Comparable> mergeSortedList(List<? extends Comparable>aList,
                                                                List<? extends Comparable>bList){

        List mergeList = new ArrayList();

        while (!aList.isEmpty() && !bList.isEmpty()) {
            if (bList.get(0).compareTo(aList.get(0)) < 0) {
                mergeList.add(bList.get(0));
                bList.remove(0);
            } else if (bList.get(0).compareTo(aList.get(0)) == 0) {
                mergeList.add(bList.get(0));
                bList.remove(0);
                mergeList.add(aList.get(0));
                aList.remove(0);
            } else if (bList.get(0).compareTo(aList.get(0)) > 0) {
                mergeList.add(aList.get(0));
                aList.remove(0);
            }
        }
        if (!aList.isEmpty()) {
            for (int i = 0; i < aList.size(); i++) {
                mergeList.add(aList.get(i));
            }
        } else if (!bList.isEmpty()) {
            for (int i = 0; i < bList.size(); i++) {
                mergeList.add(bList.get(i));
            }
        }


        return mergeList;
    }

}

