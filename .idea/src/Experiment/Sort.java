package Experiment;

import java.util.Random;

/**
 * Created by 竹韵澜 on 2017/9/24.
 */
public class Sort {
    public void sorting(int x, int y, int z){
       int temp = 0;
        if( x > y) {
            temp = y;
            y = x;
            x = temp;
        }

        if( y > z){
            temp = y;
            y = z;
            z = temp;
        }
        if( x > y){
            temp = y;
            y = x;
            x = temp;
        }
        System.out.println("从小到大排列： " + x + "， " + y + "， "+ z );
    }

    public static void main(String[] args) {
        Random random = new Random();
        int x, y, z;
        x = random.nextInt();
        y = random.nextInt();
        z = random.nextInt();
        System.out.println("排列之前：" + x + "， " + y + "， " + z );
        Sort sort = new Sort();
        sort.sorting(x ,y ,z );
    }
}
