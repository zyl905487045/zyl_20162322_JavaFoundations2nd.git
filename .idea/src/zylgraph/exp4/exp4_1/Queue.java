package zylgraph.exp4.exp4_1;

/**
 * Created by 竹韵澜 on 2017/11/20.
 */
public class Queue {
    private int[] values;
    private int begin = -1;
    private int end = -1;

    public Queue(int size){
        values = new int[size];
    }

    public void push(int value){
        values[++begin] = value;
    }

    int pop(){
        return values[++end];
    }

    public boolean isEmpty(){
        return begin == end;
    }
}
