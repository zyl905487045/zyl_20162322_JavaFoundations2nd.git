package zylgraph.exp4.exp4_3;

import java.util.List;

import static zylgraph.exp4.exp4_3.FloydInGraph.INF;

/**
 * Created by 竹韵澜 on 2017/11/20.
 */
public class CreateNet {
    public static void main(String[] args) {
        FloydInGraph graph=new FloydInGraph(5);
        int[][] matrix={
                {INF,30,INF,10,50},
                {INF,INF,60,INF,INF},
                {INF,INF,INF,INF,INF},
                {INF,INF,INF,INF,30},
                {50,INF,40,INF,INF},
        };
        int begin=0;
        int end=4;
        graph.findCheapestPath(begin,end,matrix);
        List list=graph.result;
        System.out.println(begin+" to "+end+",the cheapest path is:");
        System.out.println(list.toString());
        System.out.println(graph.dist[begin][end]);
    }
}
