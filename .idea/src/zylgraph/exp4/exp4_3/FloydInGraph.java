package zylgraph.exp4.exp4_3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 竹韵澜 on 2017/11/20.
 */
public class FloydInGraph {
    public static int INF=Integer.MAX_VALUE;
    //dist[i][j]=INF<==>i 和 j之间没有边
    int[][] dist;
    //顶点i 到 j的最短路径长度，初值是i到j的边的权重
    private int[][] path;
    public List< Integer> result=new ArrayList< Integer>();

    public FloydInGraph(int size){
        this.path=new int[size][size];
        this.dist=new int[size][size];
    }

    public void findCheapestPath(int begin,int end,int[][] matrix){
        floyd(matrix);
        result.add(begin);
        findPath(begin,end);
        result.add(end);
    }

    public void findPath(int i,int j){
        int k=path[i][j];
        if(k==-1)return;
        findPath(i,k);   //递归
        result.add(k);
        findPath(k,j);
    }
    public  void floyd(int[][] matrix){
        int size=matrix.length;
        //initialize dist and path
        for(int i=0;i< size;i++){
            for(int j=0;j< size;j++){
                path[i][j]=-1;
                dist[i][j]=matrix[i][j];
            }
        }
        for(int k=0;k< size;k++){
            for(int i=0;i< size;i++){
                for(int j=0;j< size;j++){
                    if(dist[i][k]!=INF&&
                            dist[k][j]!=INF&&
                            dist[i][k]+dist[k][j]< dist[i][j]){
                        dist[i][j]=dist[i][k]+dist[k][j];
                        path[i][j]=k;
                    }
                }
            }
        }

    }


}
