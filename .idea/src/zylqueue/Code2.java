package zylqueue;

import zylqueue.exceptions.EmptyCollectionException;

import java.util.LinkedList;

/**
 * Created by 竹韵澜 on 2017/10/18.
 */
public class Code2 {
    public static void main(String[] args) throws EmptyCollectionException {
        //消息的密钥
        int[] key = {5, 12, -3, 8, -9, 4, 10};
        Integer keyValue;
        String encoded = "",decoded = "";
        //待加密的字符串
        String message = "All programmers are playwrights and all " +
                "computers are lousy actors.";
        //用于存储密钥的队列
        LinkedList<Integer> KeyQueue1 = new LinkedList<>();
        LinkedList<Integer> KeyQueue2 = new LinkedList<>();

        //两个队列分别存储一份密钥，模拟消息编码者使用一份密钥，消息解码者使用一份密钥
        for (int scan = 0; scan < key.length; scan++){
            KeyQueue1.add (new Integer(key[scan]));
            KeyQueue2.add (new Integer(key[scan]));
        }

        //利用队列存储密钥使得密钥重复很容易，只要在用到每个密钥值后将其放回到队列即可
        for (int scan = 0; scan < message.length(); scan++){
            //取一个密钥
            keyValue = KeyQueue1.getFirst();
            //会将该字符移动Unicode字符集的另外一个位置
            encoded += (char)((int)message.charAt(scan) + keyValue.intValue());
            //将密钥重新存储到队列中
            KeyQueue1.add (keyValue);
        }

        System.out.println("Encoded Message:\n" + encoded +"\n");

        for (int scan = 0; scan < encoded.length(); scan++){
            keyValue = KeyQueue2.getFirst();
            decoded += (char)((int)encoded.charAt(scan) - keyValue.intValue());
            KeyQueue2.add(keyValue);
        }

        System.out.println("Decoded Message:\n" + decoded);
    }
}
