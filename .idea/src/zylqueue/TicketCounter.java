package zylqueue;

import zylqueue.exceptions.EmptyCollectionException;

/**
 * Created by 竹韵澜 on 2017/10/15.
 */
public class TicketCounter {
    //接受服务的时间
    final static int PROCESS = 120;
    //最多窗口数
    final static int MAX_CASHIERS = 5;
    //顾客的数量
    final static int NUM_CUSTOMERS = 10;

    public static void main(String[] args) throws EmptyCollectionException {
        Customer customer;
        //存储顾客的队列
        LinkedQueue<Customer> customerQueue = new LinkedQueue<Customer>();
        int[] cashierTime = new int[MAX_CASHIERS];
        int totalTime, averageTime, departs;

        //该循环决定了每遍模拟时用了多少个售票口
        for (int cashiers=0; cashiers < MAX_CASHIERS; cashiers++){
            //将售票口的服务时间初始化为 0
            for (int count=0; count < cashiers; count++)
                cashierTime[count] = 0;

            //往costomerQueue存储顾客，模拟每隔15分钟来一个顾客
            for (int count=1; count <= NUM_CUSTOMERS; count++)
                customerQueue.enqueue(new Customer(count*15));
            //初始化总的服务时间为0
            totalTime = 0;

            //开始服务
            while (!(customerQueue.isEmpty()))
            {
                for (int count=0; count <= cashiers; count++)
                {
                    if (!(customerQueue.isEmpty()))
                    {
                        //取出一位顾客
                        customer = (Customer) customerQueue.dequeue();
                        //顾客来的时间与售票口的服务时间相比
                        if (customer.getArrivalTime() > cashierTime[count])
                            //表示空闲，可以进行服务
                            departs = customer.getArrivalTime() + PROCESS;
                        else
                            //无空闲则需排队等待
                            departs = cashierTime[count] + PROCESS;
                        //保存用户的离开时间
                        customer.setDepartureTime (departs);
                        //设置该售票口的服务时间
                        cashierTime[count] = departs;
                        //计算总的服务时间
                        totalTime += customer.totalTime();
                    }
                }
            }

            averageTime = totalTime / NUM_CUSTOMERS;
            System.out.println ("Number of cashiers: " + (cashiers+1));
            System.out.println ("Average time: " + averageTime + "\n");
        }
    }
}
