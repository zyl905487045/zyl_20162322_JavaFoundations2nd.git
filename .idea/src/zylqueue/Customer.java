package zylqueue;

/**
 * Created by 竹韵澜 on 2017/10/15.
 */
public class Customer {
    //arrivalTime跟踪顾客抵达售票口的时间，departureTime跟踪顾客买票后离开售票口的时间
    private int arrivalTime, departureTime;

    public Customer (int arrives){
        arrivalTime = arrives;
        departureTime = 0;
    }

    public int getArrivalTime(){
        return arrivalTime;
    }

    public void setDepartureTime (int departs){
        departureTime = departs;
    }

    public int getDepartureTime(){
        return departureTime;
    }

    //顾客买票所花的总时间就是离开时间 - 抵达时间
    public int totalTime(){
        return departureTime - arrivalTime;
    }
}
