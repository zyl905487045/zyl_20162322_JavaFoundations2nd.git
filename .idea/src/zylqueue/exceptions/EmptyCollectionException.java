package zylqueue.exceptions;

/**
 * Created by 竹韵澜 on 2017/10/15.
 */
public class EmptyCollectionException extends RuntimeException {
    public EmptyCollectionException (String collection)
    {
        super ("The " + collection + " is empty.");
    }
}
