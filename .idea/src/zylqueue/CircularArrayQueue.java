package zylqueue;

import zylqueue.exceptions.EmptyCollectionException;

/**
 * Created by 竹韵澜 on 2017/10/15.
 */
public class CircularArrayQueue<T> implements Quene<T> {

    private final int DEFAULT_CAPACITY = 10;
    private int front, rear, count;
    private T[] queue;

    public CircularArrayQueue(){
        front = rear = count = 0;
        queue = (T[])(new Object[DEFAULT_CAPACITY]);
    }

    public CircularArrayQueue(int initialCapacity){
        front = rear = count = 0;
        queue = (T[]) new Object[initialCapacity];
    }

    /**
     * enqueue操作：通常，一个元素入列后，rear的值要递增，但当enqueue操作填充了数组的最后一个单元时，
     * rear必须设为0，表面下一个元素应该存储在索引0处，下面给出计算rear值的公式：
     * rear = （rear + 1） % queue.length;（queue是存储队列的数组名）
     * */
    @Override
    public void enqueue(T element) {
        if (count == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;
        count++;
    }

    /**
     * 当数组中的所有单元都已填充，就需要进行扩容，
     * 注意：已有数组的元素必须按其在队列中的正确顺序（而不是它们在数组中的顺序）复制到新数组中
     * */
    private void expandCapacity() {
        //增加为原容量的2倍
        T[] larger = (T[])(new Object[queue.length * 2]);

        //新数组中从索引0处开始按队列的正确顺序进行填充元素
        for (int index = 0; index <count; index++)
            larger[index] = queue[(front+index) % queue.length];

        front = 0;
        rear = count;
        queue = larger;
    }

    @Override
    /**
     * 一个元素出列后，front的值要递减，进行足够的dequeue操作后，front的值将到达数组的最后一个索引处，当最大索引处的元素被
     * 删除后，front的值必须设置为0而不是递减，在enqueue操作中用于设置rear值的计算，也可以用来设置dequeue操作的front值
     * */
    public T dequeue() throws EmptyCollectionException {
        if(isEmpty()){
            throw new EmptyCollectionException("queue");
        }
        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;
        count--;
        return result;
    }

    @Override
    public T first() throws EmptyCollectionException {
        if(isEmpty()){
            throw new EmptyCollectionException("queue");
        }
        return queue[front];
    }

    @Override
    public boolean isEmpty() {
        return count == 0 ? true : false;
    }


    @Override
    public int size() {
        return count;
    }

}
