package zylqueue;

import zylqueue.exceptions.EmptyCollectionException;

/**
 * Created by 竹韵澜 on 2017/10/15.
 */
public class LinkedQueue<T> implements Quene {
    private int count;
    private LinearNode<T> front, rear;


    public LinkedQueue()
    {
        //生成一个头结点,让头指针和尾指针都指向它
        front = rear = new LinearNode<T> (null);
        count = 0;

    }
    @Override
    public void enqueue(Object element) {
        LinearNode<T> node = new LinearNode<T>((T) element);

        if (isEmpty())
            front = node;
        else
            rear.setNext (node);

        rear = node;
        count++;
    }

    @Override
    public Object dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = front.getElement();
        front = front.getNext();
        count--;

        //如果此时队列为空，则要将rear引用设置为null,front也为null，但由于front设置为链表的next引用，已经有处理
        if (isEmpty())
            rear = null;

        return result;
    }

    @Override
    public Object first() {
        if (isEmpty())
            throw new EmptyCollectionException ("queue");

        return front.getElement();
    }

    @Override
    public boolean isEmpty() {
        return (count == 0);
    }

    @Override
    public int size() {
        return count;
    }

    public String toString()
    {
        String result = "";
        LinearNode<T> current = front;

        while (current != null)
        {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}
