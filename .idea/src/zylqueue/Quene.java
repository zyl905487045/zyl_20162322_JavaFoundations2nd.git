package zylqueue;

/**
 * Created by 竹韵澜 on 2017/10/15.
 */
public interface Quene<T> {
    public void enqueue(T element);
    public T dequeue();
    public T first();
    public boolean isEmpty();
    public int size();
    public String toString();
}
