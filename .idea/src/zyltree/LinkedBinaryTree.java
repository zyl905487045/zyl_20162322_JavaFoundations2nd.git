package zyltree;

import exceptions.EmptyCollectionException;
import zylqueue.LinkedQueue;

import java.util.Iterator;

/**
 * Created by 竹韵澜 on 2017/10/19.
 */
public class LinkedBinaryTree<T> implements BinaryTree<T> {
    protected BTNode<T> root;

    public LinkedBinaryTree(){
        root = null;
    }

    public LinkedBinaryTree (T element){
        root = new BTNode<T>(element);
    }

    public  LinkedBinaryTree (T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right){
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    @Override
    public T getRootElement() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation" +
                    "failed. The tree is empty.");
        return root.getElement();
    }

    @Override
    public BinaryTree<T> getLeft() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation" +
                    "failed. The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    @Override
    public BinaryTree<T> getRight() {
        if (root == null)
            throw new EmptyCollectionException("Get right operation" +
                    "failed. The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();
        return result;
    }

    /* 如果二叉树包含与指定元素匹配的元素，则返回true，否则返回false。
    *  关键在于遍历二叉树 */
    @Override
    public boolean contains(T target) {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        return (node != null);
    }

    @Override
    public T find(T target) {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new EmptyCollectionException("Get left operation" +
                    "failed. The tree is empty.");

        return root.getElement();
    }

    /* 如果二叉树不包含元素，则返回true，否则返回false。 */
    @Override
    public boolean isEmpty() {
        int result = 0;

        if (root != null)
            result = root.count();

        return (result == 0);
    }

    @Override
    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    @Override
    public Iterator<T> preorder() {
        ArrayIterator<T> iter = new ArrayIterator<>();

        if (root != null)
            root.preorder(iter);

        return iter;
    }

    @Override
    public Iterator<T> inorder() {
       ArrayIterator<T> iter = new ArrayIterator<T>();

       if (root != null)
          root.inorder (iter);

        return iter;
    }

    @Override
    public Iterator<T> postorder() {
        ArrayIterator<T> iter = new ArrayIterator<T>();

        if (root != null)
            root.postorder(iter);

        return iter;
    }

    @Override
    public Iterator<T> levelorder() {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayIterator<T> iter = new ArrayIterator<T>();

        if (root != null){
            queue.enqueue(root);
            while (!queue.isEmpty()){
                BTNode<T> current = (BTNode<T>) queue.dequeue();

                iter.add (current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }
        return iter;
    }

    @Override
    public Iterator<T> iterator() {
        return (Iterator<T>) inorder();
    }
}
