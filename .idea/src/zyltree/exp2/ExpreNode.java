package zyltree.exp2;

/**
 * Created by 竹韵澜 on 2017/10/27.
 */
public class ExpreNode {
    private  String expre;
    private ExpreNode lchild;
    private ExpreNode rchild;

    public ExpreNode(){

    }

    public ExpreNode(String expre){
        this.expre = expre;
    }

    public ExpreNode(String expre, ExpreNode lchild, ExpreNode rchild){
        super();
        this.expre = expre;
        this.lchild = lchild;
        this.rchild = rchild;
    }

    public String getExpre(){
        return expre;
    }

    public ExpreNode getLchild(){
        return lchild;
    }

    public ExpreNode getRchild(){
        return rchild;
    }
}
