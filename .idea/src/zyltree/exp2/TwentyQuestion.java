package zyltree.exp2;

import zyltree.LinkedBinaryTree;

import java.util.Scanner;

/**
 * Created by 竹韵澜 on 2017/10/26.
 */
public class TwentyQuestion {
    private LinkedBinaryTree<String> tree;

    public TwentyQuestion() {
        String q1 = "是否有歌词？";
        String q2 = "是英文歌吗？";
        String q3 = "是Westlife的歌吗？";
        String q4 = "是粤语歌吗？";
        String q5 = "歌名是一个字吗？";
        String q6 = "是河图的吗？";
        String q7 = "是古典音乐吗？";

        String a1 = "《Seasons In The Sun》";
        String a2 = "《Try Everthing》";
        String a3 = "《我（粤语）》";
        String a4 = "《岁月如歌》";
        String a5 = "《琴师》";
        String a6 = "《棠梨煎雪》";
        String a7 = "《Snata for Two Pianos in D Major, K 448 375a》";
        String a8 = "《アシタカとサン》";

        LinkedBinaryTree<String> n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15;

        n12 = new LinkedBinaryTree<String>(a3);
        n13 = new LinkedBinaryTree<String>(a4);
        n10 = new LinkedBinaryTree<String>(q5, n12, n13);

        n14 = new LinkedBinaryTree<String>(a5);
        n15 = new LinkedBinaryTree<String>(a6);
        n11 = new LinkedBinaryTree<String>(q6, n14, n15);

        n5 = new LinkedBinaryTree<String>(q4, n10, n11);

        n8 = new LinkedBinaryTree<String>(a1);
        n9 = new LinkedBinaryTree<String>(a2);
        n4 = new LinkedBinaryTree<String>(q3, n8, n9);

        n2 = new LinkedBinaryTree<String>(q2, n4, n5);

        n6 = new LinkedBinaryTree<String>(a7);
        n7 = new LinkedBinaryTree<String>(a8);
        n3 = new LinkedBinaryTree<String>(q7, n6, n7);

        tree = new LinkedBinaryTree<String>(q1, n2, n3);
    }

    public void guess() {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println("假设你的歌单里有一下歌曲：《Seasons In The Sun》、" +
                "《Try Everthing》、《我（粤语）》、《岁月如歌》、" +
                "《琴师》、《棠梨煎雪》、《Snata for Two Pianos in D Major, K 448 375a》、" +
                "《アシタカとサン》");
        System.out.println("请让我猜猜你现在听的是哪首歌~我会问你一些一般疑问句，你只能回答是或否哟~");
        System.out.println();

        while (current.size() > 1){
            System.out.println(current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("是"))
                current = (LinkedBinaryTree<String>) current.getLeft();
            else
                current = (LinkedBinaryTree<String>) current.getRight();
        }

        System.out.println(current.getRootElement());
    }

}
