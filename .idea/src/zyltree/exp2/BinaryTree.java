package zyltree.exp2;

/**
 * Created by 竹韵澜 on 2017/10/26.
 */
public class BinaryTree {
    public String value;
    public BinaryTree leftNode;
    public BinaryTree rightNode;
    BinaryTree(String x) {
        value = x;
    }
}
