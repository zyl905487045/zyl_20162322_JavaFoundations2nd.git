package zyltree.exp2;

import zyltree.LinkedBinaryTree;

import java.util.Arrays;

/**
 * Created by 竹韵澜 on 2017/10/23.
 */
public class CreateTree {
    public static BinaryTree startBuildTree(String[] preorder, String[] inorder) throws Exception {
        //异常判断
        if(preorder==null || inorder==null){
            return null;
        }
        if(preorder.length!=inorder.length){
            throw new Exception("不满足条件的非法输入！");
        }

        BinaryTree root=null;
        for(int i = 0; i < inorder.length ; i++){
            if(inorder[i].equals(preorder[0])){
                root=new BinaryTree(preorder[0]);
                System.out.println(preorder[0]);

                root.leftNode=startBuildTree(
                        Arrays.copyOfRange(preorder, 1, i+1),
                        Arrays.copyOfRange(inorder, 0, i));
                root.rightNode=startBuildTree(
                        Arrays.copyOfRange(preorder, i+1, preorder.length),
                        Arrays.copyOfRange(inorder, i+1, inorder.length));
            }
        }


        return root;
    }

}


