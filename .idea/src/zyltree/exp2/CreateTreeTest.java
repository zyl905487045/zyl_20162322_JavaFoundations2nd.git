package zyltree.exp2;

/**
 * Created by 竹韵澜 on 2017/10/26.
 */
public class CreateTreeTest {
    public static void main(String[] args) throws Exception {
        String preorder0 = "A,B,D,H,I,E,J,M,N,C,F,G,K,L";
        String inorder0 = "H,D,I,B,E,M,J,N,A,F,C,K,G,L";
        String[] preorder = preorder0.split(",");
        String[] inorder = inorder0.split(",");
        CreateTree createTree = new CreateTree();
        createTree.startBuildTree(preorder,inorder);
    }
}
