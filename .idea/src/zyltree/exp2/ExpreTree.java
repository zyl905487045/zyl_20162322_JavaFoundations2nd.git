package zyltree.exp2;

import java.util.ArrayList;

/**
 * Created by 竹韵澜 on 2017/10/26.
 */
public class ExpreTree {
    private String s="";
    private ExpreNode root;

    public void createExpreTree(String expre){
        //声明一个数组列表，存放的操作符，加减乘除
        ArrayList<String> operList = new ArrayList<String>();
        //声明一个数组列表，存放节点的数据
        ArrayList<ExpreNode> numList = new ArrayList<ExpreNode>();

        //第一，辨析出操作符与数据，存放在相应的列表中
        for(int i=0 ; i<expre.length() ; i++){
            char ch = expre.charAt(i);          //取出字符串的各个字符
            if(ch >= '0' && ch <= '9'){
                s+=ch;
            }
            else{
                numList.add(new ExpreNode(s));
                s="";
                operList.add(ch+"");
            }
        }

        //把最后的数字加入到数字节点中
        numList.add(new ExpreNode(s));

        while(operList.size()>0){    //第三步，重复第二步，直到操作符取完为止
            // 第二，取出前两个数字和一个操作符，组成一个新的数字节点
            ExpreNode left = numList.remove(0);
            ExpreNode right = numList.remove(0);
            String oper = operList.remove(0);

            ExpreNode node = new ExpreNode(oper,left,right);
            numList.add(0,node);       //将新生的节点作为第一个节点，同时以前index=0的节点变为index=1
        }
        //第四步，让根节点等于最后一个节点
        root = numList.get(0);

    }

    public void output(){
        output(root);      //从根节点开始遍历
    }

    public void output(ExpreNode expreNode){
        if(expreNode.getLchild()!=null){
            output(expreNode.getLchild());
        }
        System.out.print(expreNode.getExpre());
        if(expreNode.getRchild()!=null){
            output(expreNode.getRchild());
        }
    }
}
