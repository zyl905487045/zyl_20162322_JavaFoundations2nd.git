package zyltree;

import java.util.Iterator;

/**
 * Created by 竹韵澜 on 2017/10/19.
 */
public interface BinaryTree<T> extends Iterable<T> {
    /* 返回根结点的值 */
    public T getRootElement();
    /* 返回左子结点的值 */
    public BinaryTree<T> getLeft();
    /* 返回右子结点的值 */
    public BinaryTree<T> getRight();
    /* 如果二叉树包含与指定元素匹配的元素，则返回true，否则返回false。 */
    public boolean contains(T target);
    /* 返回对与指定目标匹配的树中的元素的引用。 */
    public T find(T target);
    /* 如果二叉树不包含元素，则返回true，否则返回false。 */
    public boolean isEmpty();
    /* 返回此二叉树中的元素数。 */
    public int size();
    /* 返回二叉树的字符串形式。*/
    public String toString();
    /* 先序遍历 */
    public Iterator<T> preorder();
    /* 中序遍历 */
    public Iterator<T> inorder();
    /* 后序遍历 */
    public Iterator<T> postorder();
    /* 层序遍历 */
    public Iterator<T> levelorder();
}
