package zyltree;

/**
 * Created by 竹韵澜 on 2017/10/23.
 */
public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree<Integer> linkedBinaryTree1 = new LinkedBinaryTree<Integer>(1);
        LinkedBinaryTree<Integer> linkedBinaryTree2 = new LinkedBinaryTree<Integer>(2);
        LinkedBinaryTree<Integer> linkedBinaryTree3 = new LinkedBinaryTree<Integer>(3,linkedBinaryTree1,linkedBinaryTree2);

        linkedBinaryTree3.preorder();
        System.out.println(linkedBinaryTree3.getRight().getRootElement());
        System.out.println(linkedBinaryTree3.contains(1));
        System.out.println(linkedBinaryTree3.contains(4));
        System.out.println(linkedBinaryTree3.preorder());
        System.out.println(linkedBinaryTree3.postorder());
    }
}
