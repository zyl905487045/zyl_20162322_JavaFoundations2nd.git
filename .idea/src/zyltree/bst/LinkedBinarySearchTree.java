package zyltree.bst;

import exceptions.ElementNotFoundException;
import zyltree.LinkedBinaryTree;

/**
 * Created by 竹韵澜 on 2017/10/26.
 */

//*******************************************************************
//  LinkedBinarySearchTree.java       Java Foundations
//
//  Implements the binary tree using a linked representation.
//*******************************************************************
public class LinkedBinarySearchTree<T extends Comparable<T>>
        extends LinkedBinaryTree<T> implements BinarySearchTree<T> {
    //-----------------------------------------------------------------
    //  Creates an empty binary search tree.
    //-----------------------------------------------------------------
    public LinkedBinarySearchTree(BSTNode<Integer> bstNode) {
        super();
    }

    //-----------------------------------------------------------------
    //  Creates a binary search tree with the specified element at its
    //  root.
    //-----------------------------------------------------------------
    public LinkedBinarySearchTree (T element) {
        root = new BSTNode<T>(element);
    }

    //-----------------------------------------------------------------
    //  Adds the specified element to this binary search tree.
    //-----------------------------------------------------------------
    @Override
    public void add(T element) {
        if (root == null)
            root = new BSTNode<T>(element);
        else
            ((BSTNode)root).add(element);
    }

    @Override
    public T findMin() {
        if (root == null)
            return null;

        if (root != null && root.getLeft() == null)
            return root.getElement();
        else
        while (root != null && root.getLeft() != null){
            root = root.getLeft();
            if (root.getLeft() == null){
                    break;
            }
        }
        return root.getElement();

    }
    
    @Override
    public T findMax() {
        if (root == null)
            return null;

        if (root != null && root.getRight() == null)
            return root.getElement();

        while (root != null && root.getRight() != null){
            root = root.getRight();
            if (root.getRight() == null){
                break;
            }
        }
        return root.getElement();
    }

    //-----------------------------------------------------------------
    //  Removes and returns the element matching the specified target
    //  from this binary search tree. Throws an ElementNotFoundException
    //  if the target is not found.
    //-----------------------------------------------------------------
    @Override
    public T remove(T target) {
        BSTNode<T> node = null;

        if (root != null)
            node = ((BSTNode)root).find(target);

        if (node == null)
            throw new ElementNotFoundException ("Remove operation failed. "
                    + "No such element in tree.");

        root = ((BSTNode)root).remove(target);

        return node.getElement();
    }
}
