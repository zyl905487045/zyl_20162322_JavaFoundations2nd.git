package zylstack;

import java.util.Scanner;

/**
 * Created by 竹韵澜 on 2017/10/11.
 */

/*
*  PostfixEvaluator.java     Java Foundations
*
*  Repersents an integer evaluator of postfix expretions. Assumes the operands are constants.
*/

public class PostfixEvaluator {
    private final char ADD = '+', SUBTRACT = '-';
    private final char MULTIPLY = '*', DIVIDE = '/';

    private LinkedStack<Integer> stack;

    public int evaluate (String expr){
        int op1, op2, result = 0;
        String token;
        Scanner tokenier = new Scanner(expr);

        while (tokenier.hasNext()){
            token = tokenier.next();

            if (isOperator(token)){
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evalSingleOp (token.charAt(0), op1 , op2);
                stack.push(result);
            }
            else
                stack.push (Integer.parseInt(token));
        }
        return result;
    }

    private boolean isOperator (String token){
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    private int evalSingleOp (char operation, int op1, int op2){
        int result = 0;

        switch (operation){
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
                break;
        }
        return result;
    }

}
