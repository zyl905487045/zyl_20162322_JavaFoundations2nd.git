package zylstack;

/**
 * Created by 竹韵澜 on 2017/10/9.
 */
public interface Stack<T> {
    public void push(T element);
    public T pop();
    public T peek();
    public boolean isEmpty();
    public int size();
    public String toString();
}
